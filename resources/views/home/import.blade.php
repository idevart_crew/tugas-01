<form method="POST" action="{{ route('upload') }}" enctype="multipart/form-data">
    @csrf
    <input type="file" name="file">
    <button type="submit" class="btn btn-sm btn-primary">
        <i class="fa fa-upload"></i>&nbsp;
        Import Dataset
    </button>
</form>