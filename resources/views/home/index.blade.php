@extends('layouts.app')

@section('content')
<section class="content">
    <div class="container-fluid">
        <div class="card">
            <div class="card-body">
            
                @include('home.import')
                <hr/>

                @include('home.dataset')
                <hr/>

                @include('home.procces-form')
                <hr/>

                @include('home.result')
                <hr/>

                @include('home.comparison')
                {{-- @include('home.tree') --}}
                <hr/>

            </div>
        </div>
    </div>
</section>
@endSection