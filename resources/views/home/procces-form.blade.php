<div id="accordion-procces">
    <div class="card">
        <div class="card-header bg-primary" role="tab" id="heading-procces">
            <a href="javascript:void(0)" data-toggle="collapse" data-target="#collapse-procces" aria-expanded="true" aria-controls="collapse-procces">
                <h4 class="mb-0">Proses Perhitungan</h4>
            </a>
        </div>

        <div id="collapse-procces" class="collapse hide" aria-labelledby="heading-procces" data-parent="#accordion-procces">
            <div class="card-body">

                <form method="POST" action="{{ route('process') }}">
                    <div class="row">
                        <div class="col-sm-5 mr-3">
                            @csrf

                            <div class="form-group row">
                                <label for="method" class="col-sm-4 col-form-label">Pilih metode penghitungan</label>
                                <div class="col-sm-8">
                                    <select class="form-control" id="method" name="method">
                                        <option {{ old('method') == 'c45' ? 'selected' : '' }} value="c45">C45</option>
                                        <option {{ old('method') == 'knn' ? 'selected' : '' }} value="knn">KNN</option>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="method" class="col-sm-4 col-form-label">Pilih rekomendasi berdasarkan jenis kelamin</label>
                                <div class="col-sm-8">
                                    <select class="form-control" id="category" name="gender">
                                        <option {{ old('gender') == 'Laki - laki' ? 'selected' : '' }} value="Laki - laki">Laki - laki</option>
                                        <option {{ old('gender') == 'Perempuan' ? 'selected' : '' }} value="Perempuan">Perempuan</option>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="method" class="col-sm-4 col-form-label">Pilih rekomendasi berdasarkan usia</label>
                                <div class="col-sm-8">
                                    <select class="form-control" id="age" name="age">
                                        <option {{ old('age') == 'Anak-anak (Usia 8 - 15 Tahun)' ? 'selected' : '' }} value="Anak-anak (Usia 8 - 15 Tahun)">Anak-anak</option>
                                        <option {{ old('age') == 'Remaja (Usia 16 - 25 Tahun)' ? 'selected' : '' }} value="Remaja (Usia 16 - 25 Tahun)">Remaja</option>
                                        <option {{ old('age') == 'Dewasa (Usia 26 - 40 Tahun)' ? 'selected' : '' }} value="Dewasa (Usia 26 - 40 Tahun)">Dewasa</option>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="method" class="col-sm-4 col-form-label">Pilih atribut (Makanan atau minuman)</label>
                                <div class="col-sm-8">
                                    <select class="form-control" id="attribute" name="attribute">
                                        <option {{ old('attribute') == 'fav_drink' ? 'selected' : '' }} value="fav_drink">Menu Minuman</option>
                                        <option {{ old('attribute') == 'fav_food' ? 'selected' : '' }} value="fav_food">Menu Makanan</option>
                                    </select>
                                </div>
                            </div>
                            
                            <div class="form-group row">
                                <div class="col-sm-4">
                                    <button type="submit" id="proccess" class="btn btn-sm btn-primary">
                                        <i class="fa fa-cogs"></i>&nbsp;
                                        Proses Data
                                    </button>
                                </div>
                            </div>
                        </div>

                        <div id="sampleData" class="col-sm-5 ml-3" style="display:none">
                            <h4>Data Uji</h4>
                            <div class="form-group row">
                                <label for="menu" class="col-sm-4 col-form-label">Menu</label>
                                <div class="col-sm-8">
                                    <input class="form-control" name="menu" id="menu" placeholder="menu" value="{{ old('menu') }}">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="flavour" class="col-sm-4 col-form-label">Rasa</label>
                                <div class="col-sm-8">
                                    <input class="form-control" name="flavour" id="flavour" placeholder="rasa" value="{{ old('flavour') }}">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="total" class="col-sm-4 col-form-label">Total</label>
                                <div class="col-sm-8">
                                    <input class="form-control" name="total" id="total" placeholder="total" value="{{ old('total') }}">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="total_true" class="col-sm-4 col-form-label">Total Merekomendasikan</label>
                                <div class="col-sm-8">
                                    <input class="form-control" name="total_true" id="total_true" placeholder="total merekomendasikan" value="{{ old('total_true') }}">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="total_false" class="col-sm-4 col-form-label">Total Tidak Merekomendasikan</label>
                                <div class="col-sm-8">
                                    <input class="form-control" name="total_false" id="total_false" placeholder="total tidak merekomendasikan" value="{{ old('total_false') }}">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="k_value" class="col-sm-4 col-form-label">Nilai K</label>
                                <div class="col-sm-8">
                                    <input class="form-control" name="k_value" id="k_value" placeholder="nilai k" value="{{ old('k_value') }}">
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            
            </div>
        </div>
    </div>
</div>

@push('bottomScript')
<script>
    $(document).ready(function() {
        var method = $('#method').val();
        if (method === 'knn') {
            $('#sampleData').show();
        } else {
            $('#sampleData').hide();
        }
        
        $('#method').change(function() {
            var method = $(this).val();
            if (method === 'knn') {
                $('#sampleData').show();
            } else {
                $('#menu').val('');
                $('#flavour').val('');
                $('#total').val('');
                $('#total_true').val('');
                $('#total_false').val('');
                $('#k_value').val('');

                $('#sampleData').hide();
            }
        });
    });
</script>
@endpush