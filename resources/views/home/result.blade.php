<div id="accordion-result">
    <div class="card">
        <div class="card-header bg-primary" role="tab" id="heading-result">
            <a href="javascript:void(0)" data-toggle="collapse" data-target="#collapse-result" aria-expanded="true" aria-controls="collapse-result">
                <h4 class="mb-0">Result</h4>
            </a>
        </div>
        
        <div id="collapse-result" class="collapse {{ isset($result) ? 'show' : 'hide' }}" aria-labelledby="heading-result" data-parent="#accordion-result">
            <div class="card-body">
        
                @if (isset($result))
                    @if (old('method') == 'knn')
                    <div class="row mt-3">
                        <div class="col-sm-12 table-responsive">
                            <table class="table table-sm">
                                <thead>
                                    <tr>
                                        <th>{{ old('attribute') == 'fav_drink' ? 'Menu Minuman' : 'Menu Makanan' }}</th>
                                        <th>{{ old('attribute') == 'fav_drink' ? 'Rasa Minuman' : 'Rasa Makanan' }}</th>
                                        <th>Jumlah Total</th>
                                        <th>Jumlah Ya</th>
                                        <th>Jumlah Tidak</th>
                                        <th>Label</th>
                                        <th>Distance</th>
                                        <th>K={{ old('k_value') }}</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($result['data'] as $knn)
                                    <tr>
                                        <td>{{ $knn->primary_attribute }}</td>
                                        <td>{{ $knn->secondary_attribute }}</td>
                                        <td>{{ $knn->total }}</td>
                                        <td>{{ $knn->total_true }}</td>
                                        <td>{{ $knn->total_false }}</td>
                                        <td>{{ $knn->label }}</td>
                                        <td>{{ $knn->distance }}</td>
                                        <td>{{ $knn->result_k }}</td>
                                    </tr>
                                    @endforeach   
                                </tbody>
                            </table>
                        </div>

                        <h4>Hasil : {{ $result['result'][0] }}</h4>
                    </div>
                    
                    @elseif (old('method') == 'c45')

                        @foreach($result as $index => $data)
                    
                            @if ($data->result)
                            <div class="row mt-3 mb-5">
                                <div class="col-sm-12">
                                    <span class="font-weight-bold">{{ $data->result }}</span>
                                </div>
                            </div>
                            @else
                            <div class="row mt-3">
                                <div class="col-sm-12 table-responsive">
                                    <table class="table table-sm">
                                        <thead>
                                            <tr>
                                                <th>NODE {{ $data->node }}</th>
                                                <th>&nbsp;</th>
                                                <th>Jumlah Total</th>
                                                <th>Jumlah Ya</th>
                                                <th>Jumlah Tidak</th>
                                                <th>Entropi</th>
                                                <th>Gain</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($data->detail as $key => $detail)
                                                <tr>
                                                    <td>{{ $detail->attribute }}</td>
                                                    <td>&nbsp;</td>
                                                    <td>{{ $detail->total }}</td>
                                                    <td>{{ $detail->total_true }}</td>
                                                    <td>{{ $detail->total_false }}</td>
                                                    <td>{{ $detail->entrophy }}</td>
                                                    <td>{{ $detail->gain }}</td>
                                                </tr>

                                                @foreach($detail->dataset as $dataset)
                                                <tr>
                                                    <td>&nbsp;</td>
                                                    <td>{{ $dataset->attribute }}</td>
                                                    <td>{{ $dataset->total }}</td>
                                                    <td>{{ $dataset->total_true }}</td>
                                                    <td>{{ $dataset->total_false }}</td>
                                                    <td>{{ $dataset->entrophy }}</td>
                                                    <td>&nbsp;</td>
                                                </tr>
                                                @endforeach

                                            @endforeach                           
                                        </tbody>
                                    </table>
                                </div>
                            </div> 
                            @endif
                        @endforeach

                    <hr/>
                    @endif
                @endif
        
            </div>
        </div>
    </div>
</div>