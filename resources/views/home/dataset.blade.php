<div id="accordion-dataset">
    <div class="card">
        <div class="card-header bg-primary" role="tab" id="heading-dataset">
            <a href="javascript:void(0)" data-toggle="collapse" data-target="#collapse-dataset" aria-expanded="true" aria-controls="collapse-dataset">
                <h4 class="mb-0">Dataset</h4>
            </a>
        </div>

        <div id="collapse-dataset" class="collapse show" aria-labelledby="heading-dataset" data-parent="#accordion-dataset">
            <div class="card-body">
                <table id="dataTable" class="display">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Jenis Kelamin</th>
                            <th>Usia</th>
                            <th>Menu Minuman</th>
                            <th>Rasa Minuman</th>
                            <th>Merekomendasikan Minuman</th>
                            <th>Menu Makanan</th>
                            <th>Rasa Makanan</th>
                            <th>Merekomendasikan Makanan</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if ($datasets && count($datasets) > 0)
                        @php $counter = 1; @endphp
                        @foreach($datasets as $dataset)
                        <tr>
                            <td>{{ $counter }}.</td>
                            <td>{{ $dataset->gender ?? '-'}}</td>
                            <td>{{ $dataset->age ?? '-' }}</td>
                            <td>{{ $dataset->fav_drink ?? '-'}}</td>
                            <td>{{ $dataset->fav_drink_flavour ?? '-' }}</td>
                            <td>{{ $dataset->recommend_drink == 1 ? 'Ya' : 'Tidak' }}</td>
                            <td>{{ $dataset->fav_food ?? '-' }}</td>
                            <td>{{ $dataset->fav_food_flavour ?? '-' }}</td>
                            <td>{{ $dataset->recommend_food == 1 ? 'Ya' : 'Tidak' }}</td>
                        </tr>
                        @php $counter++; @endphp
                        @endforeach
                        @endif
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>