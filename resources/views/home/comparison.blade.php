<div id="accordion-comparison">
    <div class="card">
        <div class="card-header bg-primary" role="tab" id="heading-comparison">
            <a href="javascript:void(0)" data-toggle="collapse" data-target="#collapse-comparison" aria-expanded="true" aria-controls="collapse-comparison">
                <h4 class="mb-0">Perbandingan</h4>
            </a>
        </div>
        
        <div id="collapse-comparison" class="collapse hide" aria-labelledby="heading-comparison" data-parent="#accordion-comparison">
           @if (isset($result_comparison))
            <div class="card-body">
                <div class="row">
                    <div class="col-12">
                        <span class="font-weight-bold">Result C45 : </span>
                        <div class="col-sm-12 table-responsive">
                            <table class="table table-sm">
                                <thead>
                                    <tr>
                                        <th>Menu</th>
                                        <th>Rasa</th>
                                        <th>Label Aktual</th>
                                        <th>Label Hasil Hitung</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($result_comparison['c45'] as $c45)
                                    <tr>
                                        <td>{{ $c45['menu'] }}</td>
                                        <td>{{ $c45['flavour'] }}</td>
                                        <td>{{ $c45['actual_label'] }}</td>
                                        <td>{{ $c45['result_label'] }}</td>
                                    </tr>
                                    @endforeach   
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-12">
                        Akurasi : {{ isset($accuracy) && isset($accuracy['c45']) ? $accuracy['c45'] : 0 }}%
                    </div>
                </div>
                
                <div class="row mt-3">
                    <div class="col-12">
                        <span class="font-weight-bold">Data KNN : </span>
                        <div class="col-sm-12 table-responsive">
                            <table class="table table-sm">
                                <thead>
                                    <tr>
                                        <th>Menu</th>
                                        <th>Rasa</th>
                                        <th>Label Aktual</th>

                                        @foreach($result_comparison['knn'][0]['result_label'] as $kValue => $result)
                                        @if ($kValue == 1 || $kValue == 5)
                                        <th>K = {{ $kValue }}</th>
                                        @endif
                                        @endforeach
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($result_comparison['knn'] as $knn)
                                    <tr>
                                        <td>{{ $knn['menu'] }}</td>
                                        <td>{{ $knn['flavour'] }}</td>
                                        <td>{{ $knn['actual_label'] }}</td>

                                        @foreach($knn['result_label'] as $key => $result)
                                        @if ($key == 1 || $key == 5)
                                        <td>{{ $result }}</td>
                                        @endif
                                        @endforeach

                                    </tr>
                                    @endforeach   
                                </tbody>
                            </table>
                        </div>                
                    </div>
                </div>

                <div class="row">
                    <div class="col-12">
                        Akurasi :
                    </div>
                </div>

                @if (isset($accuracy) && isset($accuracy['knn']))
                @foreach($accuracy['knn'] as $index => $accuracy)
                <div class="row">
                    <div class="col-1">
                        &nbsp;
                    </div>
                    <div class="col-2">
                        K = {{ $index }} : {{ $accuracy }}%
                    </div>
                </div>
                @endforeach
                @endif
            </div>
            @endif
        </div>
    </div>
</div>