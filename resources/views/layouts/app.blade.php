<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>AdminLTE 3 | Dashboard</title>

        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
        <link rel="stylesheet" href="{{ asset('plugins/fontawesome-free/css/all.min.css') }}">
        <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
        <link rel="stylesheet" href="{{ asset('plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css') }}">
        <link rel="stylesheet" href="{{ asset('plugins/icheck-bootstrap/icheck-bootstrap.min.css') }}">
        <!-- <link rel="stylesheet" href="{{ asset('plugins/jqvmap/jqvmap.min.css') }}"> -->
        <link rel="stylesheet" href="{{ asset('dist/css/adminlte.min.css') }}">
        <link rel="stylesheet" href="{{ asset('plugins/overlayScrollbars/css/OverlayScrollbars.min.css') }}">
        <link rel="stylesheet" href="{{ asset('plugins/daterangepicker/daterangepicker.css') }}">
        <link rel="stylesheet" href="{{ asset('plugins/summernote/summernote-bs4.min.css') }}">
        <link rel="stylesheet" href="https://cdn.datatables.net/1.10.24/css/jquery.dataTables.min.css">

        <style>
            th, td, .dataTables_length, .dataTables_filter { font-size: 14px; }
            .dataTables_info, .paginate_button.previous, .paginate_button.next, .paginate_button { font-size: 12px; }
        </style>

        @stack('topScript')

    </head>

    <body class="hold-transition sidebar-mini layout-fixed sidebar-collapse">
        <div class="wrapper">

            <div class="preloader flex-column justify-content-center align-items-center">
                <img class="animation__shake" src="{{ asset('dist/img/AdminLTELogo.png') }}" alt="AdminLTELogo" height="60" width="60">
            </div>

            @include('layouts.header')
            @include('layouts.navbar')

            <div class="content-wrapper">
                <div class="content-header">
                    <div class="container-fluid">
                    
                        @if (session()->has('successMessage'))
                        <div class="row mb-2">
                            <div class="col-sm-12 bg-success rounded p-3">
                                <h4>Success</h4>
                                {{ session()->get('successMessage') }}
                            </div>
                        </div>
                        @endif

                        @if ($errors->any())
                        <div class="row mb-2">
                            <div class="col-sm-12 bg-danger rounded p-3">
                                <h4>Error</h4>
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                        @endif

                        <div class="row mb-2">
                            <div class="col-sm-6">
                                <h1 class="m-0">Dashboard</h1>
                            </div>
                            <div class="col-sm-6">
                                <ol class="breadcrumb float-sm-right">
                                    <li class="breadcrumb-item"><a href="/">Home</a></li>
                                    <li class="breadcrumb-item active">Dashboard </li>
                                </ol>
                            </div>
                        </div>

                    </div>
                </div>

                @yield('content')

            </div>

            @include('layouts.footer')

        </div>

        <script src="{{ asset('plugins/jquery/jquery.min.js') }}"></script>
        <script src="{{ asset('plugins/jquery-ui/jquery-ui.min.js') }}"></script>
        <script>
            $.widget.bridge('uibutton', $.ui.button)
            $(document).ready( function () {
                $('#dataTable').DataTable({ 
                    "bSort" : false 
                });

                $('#dataTableResult').DataTable({ 
                    "bSort" : false,
                    "paging": false,
                    "searching": false,
                    "info": false
                });
            } );
        </script>
        <script src="{{ asset('plugins/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
        <script src="{{ asset('plugins/chart.js/Chart.min.js') }}"></script>
        <!-- <script src="{{ asset('plugins/sparklines/sparkline.js') }}"></script> -->
        <!-- <script src="{{ asset('plugins/jqvmap/jquery.vmap.min.js') }}"></script> -->
        <!-- <script src="{{ asset('plugins/jqvmap/maps/jquery.vmap.usa.js') }}"></script> -->
        <!-- <script src="{{ asset('plugins/jquery-knob/jquery.knob.min.js') }}"></script> -->
        <!-- <script src="{{ asset('plugins/moment/moment.min.js') }}"></script> -->
        <!-- <script src="{{ asset('plugins/daterangepicker/daterangepicker.js') }}"></script> -->
        <!-- <script src="{{ asset('plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js') }}"></script> -->
        <script src="{{ asset('plugins/summernote/summernote-bs4.min.js') }}"></script>
        <script src="{{ asset('plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js') }}"></script>
        <script src="{{ asset('dist/js/adminlte.js') }}"></script>
        <script src="{{ asset('dist/js/demo.js') }}"></script>
        <!-- <script src="{{ asset('dist/js/pages/dashboard.js') }}"></script> -->
        <script src="https://cdn.datatables.net/1.10.24/js/jquery.dataTables.min.js"></script>

        @stack('bottomScript')

    </body>
</html>
