<?php

use Illuminate\Support\Facades\Route;

use App\Models\Dataset\Dataset;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'Dashboard\DashboardController@index')->name('index');
Route::post('/upload', 'Dashboard\DashboardController@upload')->name('upload');

Route::get('/process', function() {
    return redirect()->route('index');
});
Route::post('/process', 'Dashboard\DashboardController@process')->name('process');