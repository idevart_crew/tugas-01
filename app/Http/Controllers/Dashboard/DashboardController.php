<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Imports\DatasetImport;
use App\Services\C45;
use App\Services\Knn;
use Validator;
use Excel;
use DB;
use Exception;

use App\Models\Dataset\Dataset;
use App\Models\Summary\Summary;
use App\Models\Summary\ResultComparison;
use App\Models\Summary\DatasetLabel;
use App\Models\Knn\ResultKnn;

class DashboardController extends Controller
{
    public function index() {
        $datasets = Dataset::get();
        return view('home.index', ['datasets' => $datasets]);
    }

    public function upload(Request $request) {
        $input = [
            'file' => $request->file,
            'extension' => $request->file ? strtolower($request->file->getClientOriginalExtension()) : null,
        ];

        $rules = [
            'file' => 'required|max:5000',
            'extension' => 'required|in:csv,xlsx,xls',
        ];

        $validator = Validator::make($input, $rules);
        if ($validator->fails()) return redirect()->back()->withErrors($validator);

        try {
            Excel::import(new DatasetImport, $request->file);
            return redirect()->back()->with('successMessage', 'Dataset berhasil di import');

        } catch (Exception $e) {
            return redirect()->back()->withErrors($e->getMessage());
        }
    }

    public function process(Request $request) {
        $input = $request->all();

        $rules = [
            'method' => 'required',
            'gender' => 'required',
            'age' => 'required',
            'attribute' => 'required'
        ];

        $validator = Validator::make($input, $rules);
        if ($validator->fails()) return redirect()->back()->withErrors($validator);

        switch($input['method']) {
            case 'c45':
                $summaryPid = $this->_processCountingC45($input);
                $result = Summary::whereIn('pid', $summaryPid)->get();
                break;

            case 'knn':
                $result = $this->_processCountingKnn($input);
                break;

            default: 
                $result = [];
                break;
        }

        session()->flashInput($input);
        
        ResultKnn::query()->truncate();

        $datasetLabels = DatasetLabel::where('gender', $input['gender'])
            ->where('type', $input['attribute'] == 'fav_food' ? 'food' : 'drink')
            ->where('age', $input['age'])
            ->get();

        $comparisonC45 = [];
        $comparisonKnn = [];

        $actual = [
            'true' => 0,
            'false' => 0
        ];
        
        foreach($datasetLabels as $key => $label) {
            if ($label->label == 'Rekomendasi') {
                $actual['true']++;
            } else {
                $actual['false']++;
            }

            $comparisonC45[$key]['menu'] = $label->menu;
            $comparisonC45[$key]['flavour'] = $label->flavour;
            $comparisonC45[$key]['actual_label'] = $label->label;

            $resultComparisonC45 = ResultComparison::where('type', 'c45')
                ->where('menu', $comparisonC45[$key]['menu'])
                ->where('flavour', $comparisonC45[$key]['flavour'])
                ->first();

            $comparisonC45[$key]['result_label'] = $resultComparisonC45->label ?? 'Tidak Rekomendasi';

            $comparisonKnn[$key]['menu'] = $label->menu;
            $comparisonKnn[$key]['flavour'] = $label->flavour;
            $comparisonKnn[$key]['actual_label'] = $label->label;
            
            for($i=1;$i<=10;$i++) {
                if ($i === 1) {
                    $comparisonKnn[$key]['result_label'][$i] = $label->label;       
                } else {
                    $comparisonKnn[$key]['result_label'][$i] = $this->_processCountingKnn([
                        'comparison' => true,
                        'k_value' => $i,
                        'gender' => $input['gender'],
                        'age' => $input['age'],
                        'attribute' => $input['attribute'].
                        'total'
                    ])['result'][0];
                }
            }
        }

        
        $resultComparison = [
            'c45' => $comparisonC45,
            'knn' => $comparisonKnn
        ];

        $summaryC45 = ResultComparison::where('type', 'c45')->get();
        $summaryKnn = ResultKnn::get();

        $accuracy = [
            'c45' => $this->_countAccuracy($actual['true'], $actual['false'], $summaryC45->where('label', 'Rekomendasi')->count(), $summaryC45->where('label', 'Tidak Rekomendasi')->count()),
            'knn' => [
                1 => $this->_countAccuracy($actual['true'], $actual['false'], $summaryKnn->where('k_value', 1)->where('label', 'Rekomendasi')->count(), $summaryKnn->where('k_value', 1)->where('label', 'Tidak Rekomendasi')->count()),
                5 => $this->_countAccuracy($actual['true'], $actual['false'], $summaryKnn->where('k_value', 5)->where('label', 'Rekomendasi')->count(), $summaryKnn->where('k_value', 5)->where('label', 'Tidak Rekomendasi')->count()),
            ]
        ];

        return view('home.index', [
            'datasets' => Dataset::get(),
            'result' => $result,
            'result_comparison' => $resultComparison,
            'accuracy' => $accuracy
        ]);
    }

    private function _processCountingC45(array $input) {
        $c45 = new C45($input['gender'], $input['age'], $input['attribute']);
        return $c45->processCount();
    }

    private function _processCountingKnn(array $input) {
        $knn = new Knn();
        return $knn->processCount($input);
    }

    private function _countAccuracy($actualTrue, $actualFalse, $summaryTrue, $summaryFalse) {
        $result = (($actualTrue + $actualFalse) / ($actualTrue + $actualFalse + $summaryTrue + $summaryFalse)) * 100;
        return number_format((float)$result, 2, '.', '');
    }
}


function in_array_r($needle, $haystack, $strict = false) {
    foreach ($haystack as $item) {
        if (($strict ? $item === $needle : $item == $needle) || (is_array($item) && in_array_r($needle, $item, $strict))) {
            return true;
        }
    }

    return false;
}