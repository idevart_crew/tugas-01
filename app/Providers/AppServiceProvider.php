<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Database\Query\Builder;
use Illuminate\Database\Eloquent\Builder as EloquentBuilder;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register() {
        $this->macros();
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot() {
        //
    }

    protected function macros() {
        
        /** Get sql query builder and data bindings */
        Builder::macro("queryWithBindings", function() {
            $sql = $this->toSql();
            foreach($this->getBindings() as $binding) {
                $value = is_numeric($binding) ? $binding : "'$binding'";
                $sql = preg_replace('/\?/', $value, $sql, 1);
            }

            return $sql;
        });

        /** Get eloquent query builder and data bindings */
        EloquentBuilder::macro("eloquentQueryWithBindings", function() {
            $sql = $this->toSql();
            foreach($this->getBindings() as $binding) {
                $value = is_numeric($binding) ? $binding : "'$binding'";
                $sql = preg_replace('/\?/', $value, $sql, 1);
            }

            return $sql;
        });
    }
}
