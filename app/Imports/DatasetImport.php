<?php

namespace App\Imports;

use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
use Illuminate\Database\Eloquent\Collection as EloquentCollection;

use App\Models\Dataset\Dataset;
use App\Models\Summary\DatasetLabel;

class DatasetImport implements ToCollection
{
    /**
    * @param Collection $collection
    */
    public function collection(Collection $collection) {
        try {

            \DB::beginTransaction();

            $this->_truncateTable();
            
            $collection->shift();

            $data = [];

            foreach($collection as $index => $dataset) {
                $data[$index]['gender']            = $dataset[0] ?? null;
                $data[$index]['age']               = $dataset[1] ?? null;
                $data[$index]['fav_drink']         = $dataset[2] ?? null;
                $data[$index]['fav_drink_flavour'] = $dataset[3] ?? null;
                $data[$index]['recommend_drink']   = $dataset[6] ?? null;
                $data[$index]['fav_food']          = $dataset[4] ?? null;
                $data[$index]['fav_food_flavour']  = $dataset[5] ?? null;
                $data[$index]['recommend_food']    = $dataset[7] ?? null;
                $data[$index]['created_at']        = now();
                $data[$index]['updated_at']        = now();

            }
            
            Dataset::insert($data);
            \DB::commit();
            
            $this->_assignLabel();
        
        } catch(\Exception $e) {
            \DB::rollback();
            throw new \Exception($e->getMessage());
        }
    }

    private function _truncateTable() {
        Dataset::query()->truncate();
        DatasetLabel::query()->truncate();
    }

    private function _assignLabel() {
        $listDatasetFoods = Dataset::selectRaw('gender, age, fav_food, fav_food_flavour, recommend_food, count(*) as total')
            ->groupBy(['gender', 'age', 'fav_food', 'fav_food_flavour', 'recommend_food'])
            ->orderBy('total', 'desc')
            ->get();

        foreach($listDatasetFoods as $dataset) {
            $datasetFood = DatasetLabel::firstOrCreate([
                'type' => 'food',
                'gender' => $dataset->gender,
                'age' => $dataset->age,
                'menu' => $dataset->fav_food,
                'flavour' => $dataset->fav_food_flavour,
            ], [
                'label' => $dataset->recommend_food == 1 ? 'Rekomendasi' : 'Tidak Rekomendasi',
                'total' => $dataset->total
            ]);

            if ($datasetFood->wasRecentlyCreated) {

            } else {
                if ($dataset->total == $datasetFood->total) {
                    $datasetFood->label = 'Tidak Rekomendasi'; #'Netral';
                } else if ($dataset->total > $datasetFood->total) {
                    $datasetFood->label = $dataset->recommend_food == 1 ? 'Rekomendasi' : 'Tidak Rekomendasi';
                } else if ($dataset->total < $datasetFood->total) {
                    $datasetFood->label = $datasetFood->recommend_food == 1 ? 'Rekomendasi' : 'Tidak Rekomendasi';
                }

                $datasetFood->save();
            }
        }

        $listDatasetDrinks = Dataset::selectRaw('gender, age, fav_drink, fav_drink_flavour, recommend_drink, count(*) as total')
            ->groupBy(['gender', 'age', 'fav_drink', 'fav_drink_flavour', 'recommend_drink'])
            ->orderBy('total', 'desc')
            ->get();

        foreach($listDatasetDrinks as $dataset) {
            $datasetDrink = DatasetLabel::firstOrCreate([
                'type' => 'drink',
                'gender' => $dataset->gender,
                'age' => $dataset->age,
                'menu' => $dataset->fav_drink,
                'flavour' => $dataset->fav_drink_flavour,
            ], [
                'label' => $dataset->recommend_drink == 1 ? 'Rekomendasi' : 'Tidak Rekomendasi',
                'total' => $dataset->total
            ]);

            if ($datasetDrink->wasRecentlyCreated) {
                
            } 
            else {
                if ($dataset->total == $datasetDrink->total) {
                    $datasetDrink->label = 'Tidak Rekomendasi'; #Netral';
                } else if ($dataset->total > $datasetDrink->total) {
                    $datasetDrink->label = $dataset->recommend_drink == 1 ? 'Rekomendasi' : 'Tidak Rekomendasi';
                } else if ($dataset->total < $datasetDrink->total) {
                    $datasetDrink->label = $datasetDrink->recommend_drink == 1 ? 'Rekomendasi' : 'Tidak Rekomendasi';
                }

                $datasetDrink->save();
            }
        }
    }
}
