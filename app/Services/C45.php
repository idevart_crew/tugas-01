<?php

namespace App\Services;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Builder;

use App\Models\Dataset\Dataset;
use App\Models\Summary\Summary;
use App\Models\Summary\SummaryDetail;
use App\Models\Summary\SummaryDetailDataset;
use App\Models\Summary\ResultComparison;
class C45
{
    protected $datasets;
    protected $gender;
    protected $age;
    protected $attribute;
    protected $node;

    public function __construct(string $gender, string $age, string $attribute) {
        $this->datasets = Dataset::query();
        $this->datasets->where('gender', $gender)->where('age', $age);

        $this->gender = $gender;
        $this->age = $age;
        $this->attribute = $attribute;
        $this->node = 1;

        ResultComparison::where('type', 'c45')->delete();
    }

    public function processCount() {
        $processId   = [];
        $exceptQuery = [];
        $totalRoot   = null;

        $counter = 0;
        while(true) {
            if ($totalRoot !== null && $counter >= $totalRoot) {
                break;
            }
                
            $mainProcess = $this->_mainProcess($processId, $exceptQuery, $totalRoot);
            $processId   = array_merge($processId, $mainProcess->pid);
            $exceptQuery = array_merge($exceptQuery, $mainProcess->exception);
            $totalRoot   = $mainProcess->total_root;

            $counter++;
        }

        return $processId;
    }

    private function _mainProcess($processId = [], $exceptQuery = [], $totalRoot = null) {
        $datasets = (clone $this->datasets)
            ->when(count($exceptQuery) > 0, function($subQuery) use($exceptQuery) {
                return $this->_scopeException($subQuery, $exceptQuery);
            })->get();

        $summary = Summary::create([
            'pid'  => $this->_incrementProcessId(),
            'node' => $this->_incrementNode($processId)
        ]);

        $processId[] = $summary->pid;

        /** Calculate attribute total #1 */
        $this->_calculateAttributeTotal($datasets, $summary->id);
                
        /** Calculate attribute primary #1 */
        $candidatePrimaryAttributes = $datasets->pluck($this->attribute)->unique()->toArray();
        $this->_calculateAttributePrimary($datasets, $candidatePrimaryAttributes, $summary->id);

        $secondaryAttributeNames = [
            'fav_drink'=> 'fav_drink_flavour',
            'fav_food' => 'fav_food_flavour'
        ];

        /** Calculate attribute secondary #1 */
        $candidateSecondaryAttributes = $datasets->pluck($secondaryAttributeNames[$this->attribute])->unique()->toArray();
        $this->_calculateAttributeSecondary($datasets, $candidateSecondaryAttributes, $summary->id);

        /** Looking for highest gain #1 */
        $highestGainPrimary = SummaryDetail::where('summary_id', $summary->id)
            ->where('attribute', '!=', 'Total')
            ->orderBy('gain', 'desc')
            ->first();

        /** Looking for highest entrophy #1 */
        $highestEntrophyPrimary = SummaryDetailDataset::where('summary_detail_id', $highestGainPrimary->id)
            ->orderBy('entrophy', 'desc')
            ->first();

        $attributeType = $highestEntrophyPrimary->type;
        $columnValue   = $highestEntrophyPrimary->attribute;

        $columnName = [
            'fav_drink' => [
                'primary'   => 'fav_drink',
                'secondary' => 'fav_drink_flavour'
            ],
            'fav_food' => [
                'primary'   => 'fav_food',
                'secondary' => 'fav_food_flavour'
            ]
        ];

        $datasets = $datasets->where($columnName[$this->attribute][$attributeType], $columnValue);
       
        $summary = Summary::create([
            'pid'  => $this->_incrementProcessId(),
            'node' => $this->_incrementNode($processId)
        ]);

        $processId[] = $summary->pid;

        /** Calculate attribute total #2 */
        $this->_calculateAttributeTotal($datasets, $summary->id);
                
        /** Calculate attribute primary #2 */
        $candidatePrimaryAttributes = $datasets->pluck($this->attribute)->unique()->toArray();
        $this->_calculateAttributePrimary($datasets, $candidatePrimaryAttributes, $summary->id);

        $secondaryAttributeNames = [
            'fav_drink'=> 'fav_drink_flavour',
            'fav_food' => 'fav_food_flavour'
        ];

        /** Calculate attribute secondary #2 */
        $candidateSecondaryAttributes = $datasets->pluck($secondaryAttributeNames[$this->attribute])->unique()->toArray();
        $this->_calculateAttributeSecondary($datasets, $candidateSecondaryAttributes, $summary->id);

        /** Looking for highest gain #2 */
        $highestGainSecondary = SummaryDetail::where('summary_id', $summary->id)
            ->where('attribute', '!=', 'Total')
            ->orderBy('gain', 'desc')
            ->first();

        /** Looking for highest entrophy #2 */
        $highestEntrophySecondary = SummaryDetailDataset::where('summary_detail_id', $highestGainSecondary->id)
            ->orderBy('entrophy', 'desc')
            ->first();

        /** Set recommendation */
        $recommendation = $this->_createRecommendation($summary->id);
        $summary = Summary::create([
            'pid'    => $this->_incrementProcessId(),
            'node'   => $this->_incrementNode($processId),
            'result' => 'Result : ' . $recommendation['primary'] . ' ' . $recommendation['secondary']
        ]);

        ResultComparison::create([
            'type'      => 'c45',
            'gender'    => $this->gender,
            'age'       => $this->age,
            'menu'      => $recommendation['primary'],
            'flavour'   => $recommendation['secondary'],
            'label'     => 'Rekomendasi'
        ]);

        $processId[] = $summary->pid;

        
        /** Set value total root for first process */
        $countHighestGain = SummaryDetailDataset::where('summary_detail_id', $highestGainPrimary->id)->count();
        if ($totalRoot === null) $totalRoot = $countHighestGain;

        /** Set value query exception */
        $exceptQuery[] = $highestEntrophyPrimary->attribute;

        return (object) [
            'pid'        => $processId, 
            'exception'  => $exceptQuery,
            'total_root' => $totalRoot
        ];
    }

    private function _calculateAttributeTotal(Collection $datasets, int $summaryId) {
        $total = $datasets->count();
        $attribute = $this->attribute;

        $totalTrue = $datasets->when($attribute == 'fav_drink', function($items) {
            return $items->where('recommend_drink', 1);
        })
        ->when($attribute == 'fav_food', function($items) {
            return $items->where('recommend_food', 1);
        })
        ->count();

        $totalFalse = $datasets->when($attribute == 'fav_drink', function($items) {
            return $items->where('recommend_drink', 0);
        })
        ->when($attribute == 'fav_food', function($items) {
            return $items->where('recommend_food', 0);
        })
        ->count();

        $entrophy = $this->_calculateEntrophy($total, $totalTrue, $totalFalse);

        $summaryDetail = SummaryDetail::create([
            'summary_id'    => $summaryId,
            'attribute'     => 'Total',
            'total'         => $total,
            'total_true'    => $totalTrue,
            'total_false'   => $totalFalse,
            'entrophy'      => $entrophy
        ]);
    }

    private function _calculateAttributePrimary(Collection $datasets, array $candidateAttributes, int $summaryId) {
        $columnName = $this->attribute;

        $attributeName = [
            'fav_drink'=> 'Minuman Favorite',
            'fav_food' => 'Makanan Favorite'
        ];

        $summaryDetail = SummaryDetail::create([
            'summary_id' => $summaryId,
            'attribute'  => $attributeName[$columnName]
        ]);

        $data = [];
        foreach($candidateAttributes as $key => $attribute) {
            $data[$key]['total'] = $datasets->where($columnName, $attribute)->count();

            $data[$key]['totalTrue'] = $totalTrue = $datasets->where($columnName, $attribute)
                ->when($columnName == 'fav_drink', function($items) {
                    return $items->where('recommend_drink', 1);
                })
                ->when($columnName == 'fav_food', function($items) {
                    return $items->where('recommend_food', 1);
                })
                ->count();
            
            $data[$key]['totalFalse'] = $totalFalse = $datasets->where($columnName, $attribute)
                ->when($columnName == 'fav_drink', function($items) {
                    return $items->where('recommend_drink', 0);
                })
                ->when($columnName == 'fav_food', function($items) {
                    return $items->where('recommend_food', 0);
                })
                ->count();

            $data[$key]['entrophy'] = $this->_calculateEntrophy($data[$key]['total'], $data[$key]['totalTrue'], $data[$key]['totalFalse']);

            $summaryDetailDataset = SummaryDetailDataset::create([
                'summary_detail_id' => $summaryDetail->id,
                'type'              => 'primary',
                'attribute'         => $attribute,
                'total'             => $data[$key]['total'],
                'total_true'        => $data[$key]['totalTrue'],
                'total_false'       => $data[$key]['totalFalse'],
                'entrophy'          => $data[$key]['entrophy']
            ]);
        }

        $dataTotal = SummaryDetail::where('summary_id', $summaryId)->where('attribute', 'Total')->first();
        $summaryDetail->gain = $this->_calculateGain($dataTotal, $data);
        $summaryDetail->save();
    }

    private function _calculateAttributeSecondary(Collection $datasets, array $candidateSecondaryAttributes, int $summaryId) {
        $attributeName = [
            'fav_drink' => [
                'column'    => 'fav_drink_flavour',
                'attribute' => 'Rasa Minuman'
            ],
            'fav_food'  => [
                'column'    => 'fav_food_flavour',
                'attribute' => 'Rasa Makanan'
            ]
        ];

        $columnName = $attributeName[$this->attribute]['column'];

        $summaryDetail = SummaryDetail::create([
            'summary_id' => $summaryId,
            'attribute'  => $attributeName[$this->attribute]['attribute']
        ]);

        $data = [];
        foreach($candidateSecondaryAttributes as $key => $attribute) {
            $data[$key]['total'] = $datasets->where($columnName, $attribute)->count();

            $data[$key]['totalTrue'] = $totalTrue = $datasets->where($columnName, $attribute)
                ->when($columnName == 'fav_drink_flavour', function($items) {
                    return $items->where('recommend_drink', 1);
                })
                ->when($columnName == 'fav_food_flavour', function($items) {
                    return $items->where('recommend_food', 1);
                })
                ->count();
            
            $data[$key]['totalFalse'] = $totalFalse = $datasets->where($columnName, $attribute)
                ->when($columnName == 'fav_drink_flavour', function($items) {
                    return $items->where('recommend_drink', 0);
                })
                ->when($columnName == 'fav_food_flavour', function($items) {
                    return $items->where('recommend_food', 0);
                })
                ->count();

            $data[$key]['entrophy'] = $this->_calculateEntrophy($data[$key]['total'], $data[$key]['totalTrue'], $data[$key]['totalFalse']);

            $summaryDetailDataset = SummaryDetailDataset::create([
                'summary_detail_id' => $summaryDetail->id,
                'type'              => 'secondary',
                'attribute'         => $attribute,
                'total'             => $data[$key]['total'],
                'total_true'        => $data[$key]['totalTrue'],
                'total_false'       => $data[$key]['totalFalse'],
                'entrophy'          => $data[$key]['entrophy']
            ]);
        }

        $dataTotal = SummaryDetail::where('summary_id', $summaryId)->where('attribute', 'Total')->first();
        $summaryDetail->gain = $this->_calculateGain($dataTotal, $data);
        $summaryDetail->save();
    }

    private function _calculateEntrophy(int $total, int $totalTrue, int $totalFalse) {        
        $entrophy = ((($totalTrue / $total) * -1) * (log(($totalTrue / $total), 2))) + ((($totalFalse / $total) * -1) * (log(($totalFalse / $total), 2)));
        return is_nan($entrophy) ? 0 : $entrophy;
    }
    
    private function _calculateGain($dataTotal, $dataEntrophies) {
        foreach($dataEntrophies as $dataEntrophie) {
            $dataTotal->entrophy -= ($dataEntrophie['total'] / $dataTotal->total) * $dataEntrophie['entrophy'];
        }

        return is_nan($dataTotal->entrophy) ? 0 : $dataTotal->entrophy;
    }

    private function _createRecommendation(int $summaryId) {
        $this->node = $this->node + 0.1;

        $highestPrimaryAttribute = SummaryDetailDataset::whereHas('summaryDetail', function($subQuery) use($summaryId) {
                $subQuery->where('summary_id', $summaryId);
                $subQuery->whereIn('attribute', ['Minuman Favorite', 'Makanan Favorite']);
            })
            ->orderBy('entrophy', 'desc')
            ->first();

        $highestSecondaryAttribute = SummaryDetailDataset::whereHas('summaryDetail', function($subQuery) use($summaryId) {
                $subQuery->where('summary_id', $summaryId);
                $subQuery->whereIn('attribute', ['Rasa Minuman', 'Rasa Makanan']);
            })
            ->orderBy('entrophy', 'desc')
            ->first();

        $attributeName = [
            'fav_drink' => 'Menu Minuman',
            'fav_food'  => 'Menu Makanan'
        ];

        // $result = 'Result : ' . $highestPrimaryAttribute->attribute . ' ' . $highestSecondaryAttribute->attribute;
        $result = ['primary' => $highestPrimaryAttribute->attribute, 'secondary' => $highestSecondaryAttribute->attribute];
        return $result;
    }

    private function _incrementProcessId() {
        $lastRecord = Summary::groupBy('pid')->orderBy('pid', 'desc')->first();
        $counter = $lastRecord ? ($lastRecord->pid + 1) : 1;
        return $counter;
    }

    private function _incrementNode(array $pid) {
        if (count($pid) > 0) {
            $summary = Summary::whereIn('pid', $pid)->orderBy('pid', 'desc')->first();

            return $summary->node + 0.01;
        }

        return 1.00;
    }

    private function _scopeException(Builder $query, array $value) {
        return $query->where(function($subQuery) use($value) {
            $exceptions = ['fav_drink', 'fav_drink_flavour', 'fav_food', 'fav_food_flavour'];

            foreach($exceptions as $column) {
                $subQuery->whereNotIn($column, $value);
            }
        });
    }
}