<?php

namespace App\Services;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Builder;

use App\Models\Dataset\Dataset;
use App\Models\Knn\ResultKnn;

class Knn
{
    public function processCount(array $input) {
        // ResultKnn::query()->truncate();

        // $kValue = 4;
        $kValue = $input['k_value'];

        // $input['total'] = 4;
        // $input['total_true'] = 2;
        // $input['total_false'] = 2;


        $query = Dataset::query();
        $query->where('gender', $input['gender'])->where('age', $input['age']);
                    
        if ($input['attribute'] == 'fav_drink') {
            // if (isset($input['comparison']) && $input['comparison'] === true) {
            //     $query->where('fav_drink', $input['menu'])->where('fav_drink_flavour', $input['flavour'] );
            // }

            $query->selectRaw("fav_drink AS primary_attribute, fav_drink_flavour AS secondary_attribute, COUNT(*) AS total, SUM(if(recommend_drink = '1', 1, 0)) AS total_true, SUM(if(recommend_drink = '0', 1, 0)) AS total_false")
                ->groupBy(['fav_drink', 'fav_drink_flavour'])
                ->orderBy('fav_drink', 'asc')
                ->orderBy('fav_drink_flavour', 'asc');       
        }
        else if ($input['attribute'] == 'fav_food') {
            // if (isset($input['comparison']) && $input['comparison'] === true) {
            //     $query->where('fav_food', $input['menu'])->where('fav_food_flavour', $input['flavour']);
            // }   
            
            $query->selectRaw("fav_food AS primary_attribute, fav_food_flavour AS secondary_attribute, COUNT(*) AS total, SUM(if(recommend_food = '1', 1, 0)) AS total_true, SUM(if(recommend_food = '0', 1, 0)) AS total_false")
                ->groupBy(['fav_food', 'fav_food_flavour'])
                ->orderBy('fav_food', 'asc')
                ->orderBy('fav_food_flavour', 'asc');       
        }
     
        $datasets = $query->get();

        $result = [];
        foreach($datasets as $index => $dataset) {
            $result[$index]['primary_attribute'] = $dataset->primary_attribute;
            $result[$index]['secondary_attribute'] = $dataset->secondary_attribute;
            $result[$index]['total'] = $dataset->total;
            $result[$index]['total_true'] = $dataset->total_true;
            $result[$index]['total_false'] = $dataset->total_false;
            $result[$index]['label'] = $this->_addLabel($dataset->total_true, $dataset->total_false);
            
            $input['total'] = isset($input['total']) ? $input['total'] : $dataset->total;
            $input['total_true'] = isset($input['total_true']) ? $input['total_true'] : $dataset->total_true;
            $input['total_false'] = isset($input['total_false']) ? $input['total_false'] : $dataset->total_false;

            $result[$index]['distance'] = $this->_sumDistance(
                $dataset->total, 
                $dataset->total_true,
                $dataset->total_false,
                $input['total'],
                $input['total_true'],
                $input['total_false'],
            );

            $result[$index]['k_value']  = $kValue;
            $result[$index]['result_k'] = null;
            
            $result[$index]['created_at'] = now();
            $result[$index]['updated_at'] = now();
        }

        ResultKnn::insert($result);

        $resultKnn = ResultKnn::orderBy('distance', 'asc')->take($kValue)->get();
        foreach($resultKnn as $knn) {
            $knn->result_k = $knn->label;
            $knn->save();
        }

        $knn = ResultKnn::orderBy('distance', 'asc')->get();
        return [
            'data'   => $knn,
            'result' => (clone $knn)->whereNotNull('result_k')->mode('result_k')
        ];
    }

    private function _addLabel($totalTrue, $totalFalse) {
        if ($totalTrue > $totalFalse) {
            return 'Rekomendasi';
        }

        return 'Tidak Rekomendasi';
    }

    private function _sumDistance($total, $totalTrue, $totalFalse, $sampleTotal, $sampleTotalTrue, $sampleTotalFalse) {
        return sqrt(pow(($total - $sampleTotal), 2) + pow(($totalTrue - $sampleTotalTrue), 2) + pow(($totalFalse - $sampleTotalFalse), 2));
    }
}
