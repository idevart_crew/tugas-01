<?php

namespace App\Models\Knn;

use Illuminate\Database\Eloquent\Model;

class ResultKnn extends Model
{
    protected $table = 'result_knn';
    protected $fillable = [
        'primary_attribute',
        'secondary_attribute',
        'total',
        'total_true',
        'total_false',
        'label',
        'distance',
        'k_value',
        'result_k'
    ];
}
