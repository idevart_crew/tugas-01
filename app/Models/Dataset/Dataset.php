<?php

namespace App\Models\Dataset;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Dataset extends Model
{
    use HasFactory;

    protected $table = 'dataset';
    protected $fillable = [
        'gender',
        'age',
        'fav_drink',
        'fav_drink_flavour',
        'recommend_drink',
        'fav_food',
        'fav_food_flavour',
        'recommend_food'
    ];
}
