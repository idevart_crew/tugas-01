<?php

namespace App\Models\Summary;

use Illuminate\Database\Eloquent\Model;

class SummaryDetail extends Model
{
    protected $table = 'summary_detail';
    protected $fillable = [
        'summary_id',
        'attribute',
        'total',
        'total_true',
        'total_false',
        'entrophy',
        'gain'
    ];

    public function dataset() {
        return $this->hasMany('App\Models\Summary\SummaryDetailDataset', 'summary_detail_id', 'id');
    }
}
