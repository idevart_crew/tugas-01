<?php

namespace App\Models\Summary;

use Illuminate\Database\Eloquent\Model;

class Summary extends Model
{
    protected $table = 'summary';
    protected $fillable = [
        'pid',
        'node',
        'result'
    ];

    public function detail() {
        return $this->hasMany('App\Models\Summary\SummaryDetail', 'summary_id', 'id');
    }
}
