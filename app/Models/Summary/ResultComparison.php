<?php

namespace App\Models\Summary;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ResultComparison extends Model
{
    use HasFactory;

    protected $table = 'result_comparison';
    protected $fillable = [
        'type',
        'gender',
        'age',
        'menu',
        'flavour',
        'label'
    ];
}
