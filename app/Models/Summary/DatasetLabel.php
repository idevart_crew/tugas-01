<?php

namespace App\Models\Summary;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DatasetLabel extends Model
{
    use HasFactory;

    protected $table = 'dataset_label';
    protected $fillable = [
        'type',
        'gender',
        'age',
        'menu',
        'flavour',
        'label',
        'total'
    ];
}
