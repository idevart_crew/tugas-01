<?php

namespace App\Models\Summary;

use Illuminate\Database\Eloquent\Model;

class SummaryDetailDataset extends Model
{
    protected $table = 'summary_detail_dataset';
    protected $fillable = [
        'summary_detail_id',
        'type',
        'attribute',
        'total',
        'total_true',
        'total_false',
        'entrophy',
        'gain'
    ];

    public function summaryDetail() {
        return $this->belongsTo('App\Models\Summary\SummaryDetail', 'summary_detail_id', 'id');
    }
}
