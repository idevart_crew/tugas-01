<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSummaryDetailTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('summary_detail', function (Blueprint $table) {
            $table->id();
            $table->foreignId('summary_id')->constrained('summary')->onUpdate('cascade')->onDelete('cascade');
            $table->string('attribute', 250)->nullable();	
            $table->integer('total')->nullable();	
            $table->integer('total_true')->nullable();	
            $table->integer('total_false')->nullable();	
            $table->float('entrophy',7,5)->nullable();	
            $table->float('gain', 7, 5)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('summary_detail');
    }
}
