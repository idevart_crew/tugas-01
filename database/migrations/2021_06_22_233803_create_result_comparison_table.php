<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateResultComparisonTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('result_comparison', function (Blueprint $table) {
            $table->id();
            $table->string('type', 5);
            $table->string('age', 50);
            $table->string('gender', 50);
            $table->string('menu', 100);
            $table->string('flavour', 100);
            $table->string('label', 100);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('result_comparison');
    }
}
