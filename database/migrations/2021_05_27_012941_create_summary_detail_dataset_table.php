<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSummaryDetailDatasetTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('summary_detail_dataset', function (Blueprint $table) {
            $table->id();
            $table->foreignId('summary_detail_id')->constrained('summary_detail')->onUpdate('cascade')->onDelete('cascade');
            $table->enum('type', ['primary','secondary']);	
            $table->string('attribute', 250);	
            $table->integer('total');	
            $table->integer('total_true');	
            $table->integer('total_false');	
            $table->float('entrophy',7,5)->nullable();	
            $table->float('gain', 7, 5)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('summary_detail_dataset');
    }
}
