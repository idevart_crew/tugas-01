<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDatasetTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dataset', function (Blueprint $table) {
            $table->id();
            $table->string('gender', 150);
            $table->string('age', 150);
            $table->string('fav_drink', 150);
            $table->string('fav_drink_flavour', 150);
            $table->tinyInteger('recommend_drink')->comment('1: Ya; 9: Tidak');
            $table->string('fav_food', 150);            
            $table->string('fav_food_flavour', 150);
            $table->tinyInteger('recommend_food')->comment('1: Ya; 9: Tidak');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dataset');
    }
}
