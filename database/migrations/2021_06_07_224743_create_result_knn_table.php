<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateResultKnnTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('result_knn', function (Blueprint $table) {
            $table->id();
            $table->string('primary_attribute', 50)->nullable();
            $table->string('secondary_attribute', 50)->nullable();
            $table->integer('total')->nullable()->default(0);
            $table->integer('total_true')->nullable()->default(0);
            $table->integer('total_false')->nullable()->default(0);
            $table->string('label', 50)->nullable();
            $table->float('distance', 8, 6)->nullable()->default(0);
            $table->integer('k_value')->nullable()->default(0);
            $table->string('result_k', 50)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('result_knn');
    }
}
