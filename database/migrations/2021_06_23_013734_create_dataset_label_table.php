<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDatasetLabelTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dataset_label', function (Blueprint $table) {
            $table->id();
            $table->string('type', 50);
            $table->string('age', 50);
            $table->string('gender', 50);
            $table->string('menu', 50);
            $table->string('flavour', 50);
            $table->string('label', 50);
            $table->integer('total')->nullable()->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dataset_label');
    }
}
